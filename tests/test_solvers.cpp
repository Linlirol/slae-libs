#include <gtest/gtest.h>
#if 0
#include "methods/tridiag_solver.h"
#include "types/tridiag_matrix.h"
#include "types/dense_matrix.h"
#include "methods/SIM.h"
#include "methods/Jacobi_solver.h"
#include "methods/gauss_seidel_solver.h"
#include "types/csr_matrix.h"
#include "types/vec_operations.h"
#include "methods/Chebyshev_SIM.h"
#include "methods/SOR.h"
#include "methods/SSOR.h"
#include "methods/GD.h"
#include "methods/CG.h"
#include "methods/HeavyBall.h"
#endif

#include "SLAE_lib.h"
using namespace SLAE_mini;


TEST(tridiag_solver, slae1) {
    std::array<double, 2> a = {1., 1.};
    std::array<double, 3> b = {4., 4., 4.};
    std::array<double, 2> c = {1., 1.};
    std::array<double, 3> f = {28., 7., 56.};

    tridiag_matrix<double, 3> A(a, b, c);
    auto x = tridiag_solver(A, f);

    ASSERT_DOUBLE_EQ(x[0], 8.);
    ASSERT_DOUBLE_EQ(x[1], -4.);
    ASSERT_DOUBLE_EQ(x[2], 15.);
}

TEST(tridiag_solver_dyn, slae1) {
    std::vector<double> a = {1., 1.};
    std::vector<double> b = {4., 4., 4.};
    std::vector<double> c = {1., 1.};
    std::vector<double> f = {28., 7., 56.};

    tridiag_matrix_dyn<double> A(3, a, b, c);
    auto x = tridiag_solver(A, f);

    ASSERT_DOUBLE_EQ(x[0], 8.);
    ASSERT_DOUBLE_EQ(x[1], -4.);
    ASSERT_DOUBLE_EQ(x[2], 15.);
}

TEST(tridiag_solver, slae2) {
    std::array<double, 2> a = {5., 1.};
    std::array<double, 3> b = {2., 4., -3.};
    std::array<double, 2> c = {-1., 2.};
    std::array<double, 3> f = {3., 6., 2.};

    tridiag_matrix<double, 3> A(a, b, c);

    auto x = tridiag_solver(A, f);

    ASSERT_DOUBLE_EQ(x[0], 64./43.) ;
    ASSERT_DOUBLE_EQ(x[1], -1./43.);
    ASSERT_DOUBLE_EQ(x[2], -29./43.);
}

TEST(tridiag_solver_dyn, slae2) {
    std::vector<double> a = {5., 1.};
    std::vector<double> b = {2., 4., -3.};
    std::vector<double> c = {-1., 2.};
    std::vector<double> f = {3., 6., 2.};

    tridiag_matrix_dyn<double> A(3, a, b, c);

    auto x = tridiag_solver(A, f);

    ASSERT_DOUBLE_EQ(x[0], 64./43.) ;
    ASSERT_DOUBLE_EQ(x[1], -1./43.);
    ASSERT_DOUBLE_EQ(x[2], -29./43.);
}


TEST(tridiag_solver, slae3) {
    std::array<double, 9> a = {1., 3., 6., 2., 3., 11., 10., 1., 1.};
    std::array<double, 10> b = {3.3, 5.2, 6.37, 12.03, 3., 5.6, 95., 102., 2., 1.};
    std::array<double, 9> c = {2., 1., 2., 5., 1., 1., 50., 30., 0.5};
    std::array<double, 10> f = {1., 1., 1., 1., 1., 1., 1., 1., 1., 1.};

    tridiag_matrix<double, 10> A(a, b, c);
    auto x = tridiag_solver(A, f);
    std::array<double, 10> x_ref = {362572824060./1551876085453., 355385766055./3103752170906.,
                                    265300269650./1551876085453., -335582640650./1551876085453.,
                                    1598853453829./3103752170906., -350477627981./3103752170906.,
                                    1349332630563./15518760854530., -186783139020./1551876085453.,
                                    1925442363493./4655628256359., 2730185892866./4655628256359.};
    for (std::size_t i = 0; i < 10; ++i) {
        ASSERT_DOUBLE_EQ(x[i], x_ref[i]);
    }
}

TEST(tridiag_solver_dyn, slae3) {
    std::vector<double> a = {1., 3., 6., 2., 3., 11., 10., 1., 1.};
    std::vector<double> b = {3.3, 5.2, 6.37, 12.03, 3., 5.6, 95., 102., 2., 1.};
    std::vector<double> c = {2., 1., 2., 5., 1., 1., 50., 30., 0.5};
    std::vector<double> f = {1., 1., 1., 1., 1., 1., 1., 1., 1., 1.};

    tridiag_matrix_dyn<double> A(10, a, b, c);
    auto x = tridiag_solver(A, f);
    std::vector<double> x_ref = {362572824060./1551876085453., 355385766055./3103752170906.,
                                    265300269650./1551876085453., -335582640650./1551876085453.,
                                    1598853453829./3103752170906., -350477627981./3103752170906.,
                                    1349332630563./15518760854530., -186783139020./1551876085453.,
                                    1925442363493./4655628256359., 2730185892866./4655628256359.};
    for (std::size_t i = 0; i < 10; ++i) {
        ASSERT_DOUBLE_EQ(x[i], x_ref[i]);
    }
}

TEST(SIM, dense1) {
    double A[2][2] = {
            {10, -1./2.},
            {-1./2., 10}
    };

    matrix<double, 2, 2> A_m{};
    for (auto i = 0; i < 2; ++i) {
        for (auto j = 0; j < 2; ++j) {
            A_m(i, j) = A[i][j];
        }
    }
    matrix<double, 2, 1> b{};
    b(0, 0) = 1.;
    b(1, 0) = 3.;

    matrix<double, 2, 1> x_expected{};
    x_expected(0, 0) = 46./399.;
    x_expected(1, 0) = 122./399.;

    matrix<double, 2, 1> x0{};
    x0(0, 0) = 0.;
    x0(1, 0) = 0.;

    double tolerance = 1e-10;
    double tau = 1e-2;
    matrix<double, 2, 1> x = SIM(A_m, b, x0, tolerance, tau);
    for (auto i = 0; i < 2; ++i) {
        EXPECT_NEAR(x_expected(i, 0), x(i, 0), tolerance);
    }
}

TEST(SIM, csr1) {
    csr<double> A_m(2, 2,
                    {{0, 0, 10.},
                     {0, 1, -1./2.},
                     {1, 0, -1./2.},
                     {1, 1, 10.}});

    std::vector<double> b = {1., 3.};

    std::vector<double> x_expected = {46./399., 122./399.};

    std::vector<double> x0{0., 0.};

    double tolerance = 1e-10;
    double tau = 1e-2;
    std::vector<double> x = SIM(A_m, b, x0, tolerance, tau);
    for (auto i = 0; i < 2; ++i) {
        EXPECT_NEAR(x_expected[i], x[i], tolerance);
    }
}

TEST(Jacobi_solver, csr1) {
    csr<double> A_m(2, 2,
                    {
                            {0, 0, 10.},
                            {0, 1, -1./2.},
                            {1, 0, -1./2.},
                            {1, 1, 10.}
                    }
    );
    std::vector<double> b = {1., 3.};
    std::vector<double> x_expected = {46./399., 122./399.};
    std::vector<double> x0 = {0., 0.};

    double tolerance = 1e-10;
    auto x = jacobi_solver(A_m, b, x0, tolerance);
    for (auto i = 0; i < 2; ++i) {
        EXPECT_NEAR(x_expected[i], x[i], tolerance);
    }
}

TEST(Jacobi_solver, slae1) {
    double A[2][2] = {
            {10, -1./2.},
            {-1./2., 10}
    };

    matrix<double, 2, 2> A_m{};
    for (auto i = 0; i < 2; ++i) {
        for (auto j = 0; j < 2; ++j) {
            A_m(i, j) = A[i][j];
        }
    }
    matrix<double, 2, 1> b{};
    b(0, 0) = 1.;
    b(1, 0) = 3.;

    matrix<double, 2, 1> x_expected{};
    x_expected(0, 0) = 46./399.;
    x_expected(1, 0) = 122./399.;

    matrix<double, 2, 1> x0{};
    x0(0, 0) = 0.;
    x0(1, 0) = 0.;

    double tolerance = 1e-10;
    matrix<double, 2, 1> x = jacobi_solver(A_m, b, x0, tolerance);
    for (auto i = 0; i < 2; ++i) {
        EXPECT_NEAR(x_expected(i, 0), x(i, 0), tolerance);
    }
}

TEST(Gauss_Seidel_solver, csr1) {
    csr<double> A_m(2, 2,
                    {
                            {0, 0, 10.},
                            {0, 1, -1./2.},
                            {1, 0, -1./2.},
                            {1, 1, 10.}
                    }
                    );
    std::vector<double> b = {1., 3.};
    std::vector<double> x_expected = {46./399., 122./399.};
    std::vector<double> x0 = {0., 0.};

    double tolerance = 1e-10;
    auto x = gauss_seidel_solver(A_m, b, x0, tolerance);
    for (auto i = 0; i < 2; ++i) {
        EXPECT_NEAR(x_expected[i], x[i], tolerance);
    }
}

TEST(Gauss_Seidel_solver, slae1) {
    double A[2][2] = {
            {10, -1./2.},
            {-1./2., 10}
    };

    matrix<double, 2, 2> A_m{};
    for (auto i = 0; i < 2; ++i) {
        for (auto j = 0; j < 2; ++j) {
            A_m(i, j) = A[i][j];
        }
    }
    matrix<double, 2, 1> b{};
    b(0, 0) = 1.;
    b(1, 0) = 3.;

    matrix<double, 2, 1> x_expected{};
    x_expected(0, 0) = 46./399.;
    x_expected(1, 0) = 122./399.;

    matrix<double, 2, 1> x0{};
    x0(0, 0) = 0.;
    x0(1, 0) = 0.;

    double tolerance = 1e-10;
    matrix<double, 2, 1> x = gauss_seidel_solver(A_m, b, x0, tolerance);
    for (auto i = 0; i < 2; ++i) {
        EXPECT_NEAR(x_expected(i, 0), x(i, 0), tolerance);
    }
}

TEST(Cheb_SIM, slae1) {
    std::vector<double> b = {12./35., 92./16.};
    csr<double> A(2, 2, {
            {0, 0, 2.},
            {0, 1, 1.},
            {1, 0, 1.},
            {1, 1, 3.}
    });

    std::vector<double> x0 = {10., 0.};
    std::vector<double> x_ref = {-661./700., 781./350.};
    double tolerance = 1e-10;
    std::vector<double> x = ChebyshevSIM(A, b, x0, tolerance, (std::sqrt(5.) + 5.)/2., (-std::sqrt(5.) + 5.)/2.);
    for (std::size_t i = 0; i < 2; ++i) {
        EXPECT_NEAR(x[i], x_ref[i], tolerance);
    }
}

TEST(SOR, slae1) {
    std::vector<double> b = {12./35., 92./16.};
    csr<double> A(2, 2, {
            {0, 0, 2.},
            {0, 1, 1.},
            {1, 0, 1.},
            {1, 1, 3.}
    });

    std::vector<double> x0 = {10., 0.};
    std::vector<double> x_ref = {-661./700., 781./350.};
    double tolerance = 1e-10;
    double w = 0.2;
    std::vector<double> x = SOR(A, b, x0, w, tolerance);
    for (std::size_t i = 0; i < 2; ++i) {
        EXPECT_NEAR(x[i], x_ref[i], tolerance);
    }
}

TEST(SSOR, slae1) {
    std::vector<double> b = {12./35., 92./16.};
    csr<double> A(2, 2, {
            {0, 0, 2.},
            {0, 1, 1.},
            {1, 0, 1.},
            {1, 1, 3.}
    });

    std::vector<double> x0 = {10., 0.};
    std::vector<double> x_ref = {-661./700., 781./350.};
    double tolerance = 1e-10;
    double w = 0.2;
    std::vector<double> x = SSOR(A, b, x0, w, tolerance);
    for (std::size_t i = 0; i < 2; ++i) {
        EXPECT_NEAR(x[i], x_ref[i], tolerance);
    }
}

TEST(GD, slae1) {
    std::vector<double> b = {12./35., 92./16.};
    csr<double> A(2, 2, {
            {0, 0, 2.},
            {0, 1, 1.},
            {1, 0, 1.},
            {1, 1, 3.}
    });

    std::vector<double> x0 = {10., 0.};
    std::vector<double> x_ref = {-661./700., 781./350.};
    double tolerance = 1e-10;
    std::vector<double> x = GD(A, b, x0, tolerance);
    for (std::size_t i = 0; i < 2; ++i) {
        EXPECT_NEAR(x[i], x_ref[i], tolerance);
    }
}

TEST(CG, slae1) {
    std::vector<double> b = {12./35., 92./16.};
    csr<double> A(2, 2, {
            {0, 0, 2.},
            {0, 1, 1.},
            {1, 0, 1.},
            {1, 1, 3.}
    });

    std::vector<double> x0 = {10., 0.};
    std::vector<double> x_ref = {-661./700., 781./350.};
    double tolerance = 1e-10;
    std::vector<double> x = CG(A, b, x0, tolerance);
    for (std::size_t i = 0; i < 2; ++i) {
        EXPECT_NEAR(x[i], x_ref[i], tolerance);
    }
}

TEST(HeavyBall, slae1) {
    std::vector<double> b = {12./35., 92./16.};
    csr<double> A(2, 2, {
            {0, 0, 2.},
            {0, 1, 1.},
            {1, 0, 1.},
            {1, 1, 3.}
    });

    std::vector<double> x0 = {10., 0.};
    std::vector<double> x_ref = {-661./700., 781./350.};
    double tolerance = 1e-10;
    std::vector<double> x = HeavyBall(A, b, x0, tolerance);
    for (std::size_t i = 0; i < 2; ++i) {
        EXPECT_NEAR(x[i], x_ref[i], tolerance);
    }
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
