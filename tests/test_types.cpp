#include <gtest/gtest.h>

#if 0
#include "types/tridiag_matrix.h"
#include "types/dense_matrix.h"
#include "types/csr_matrix.h"
#include "types/vec_operations.h"
#endif
#include "SLAE_lib.h"

using namespace SLAE_mini;

TEST(tridiag_matrix, brackets1) {
    std::array<double, 2> a = {1., 1.};
    std::array<double, 3> b = {4., 4., 4.};
    std::array<double, 2> c = {1., 1.};
    std::array<double, 3> f = {28., 7., 56.};

    tridiag_matrix<double, 3> A(a, b, c);
    std::array<double, 7> abc = {1., 1., 4., 4., 4., 1., 1.};

    for (std::size_t i = 0; i < 7; ++i) {
        ASSERT_EQ(A(i), abc[i]);
    }
}

TEST(tridiag_matrix_dyn, brackets1) {
    std::vector<double> a = {1., 1.};
    std::vector<double> b = {4., 4., 4.};
    std::vector<double> c = {1., 1.};
    std::vector<double> f = {28., 7., 56.};

    tridiag_matrix_dyn<double> A(3, a, b, c);
    std::vector<double> abc = {1., 1., 4., 4., 4., 1., 1.};

    for (std::size_t i = 0; i < 7; ++i) {
        ASSERT_EQ(A(i), abc[i]);
    }
}

TEST(tridiag_matrix, brackets2) {
    std::array<double, 2> a = {5., 1.};
    std::array<double, 3> b = {2., 4., -3.};
    std::array<double, 2> c = {-1., 2.};
    std::array<double, 3> f = {3., 6., 2.};

    tridiag_matrix<double, 3> A(a, b, c);
    std::array<double, 7> abc = {5., 1., 2., 4., -3., -1., 2.};

    for (std::size_t i = 0; i < 7; ++i) {
        ASSERT_EQ(A(i), abc[i]);
    }
}

TEST(tridiag_matrix_dyn, brackets2) {
    std::vector<double> a = {5., 1.};
    std::vector<double> b = {2., 4., -3.};
    std::vector<double> c = {-1., 2.};
    std::vector<double> f = {3., 6., 2.};

    tridiag_matrix_dyn<double> A(3, a, b, c);
    std::vector<double> abc = {5., 1., 2., 4., -3., -1., 2.};

    for (std::size_t i = 0; i < 7; ++i) {
        ASSERT_EQ(A(i), abc[i]);
    }
}

TEST(dense_matrix, fill) {
    matrix<double, 2, 3> m{};
    double arr1[2][3] {
            {4.2, 5., 23.2323},
            {0., 1.55, 6.}
    };
    for (std::size_t i = 0; i < 2; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m(i, j) = arr1[i][j];
        }
    }
    ASSERT_DOUBLE_EQ(m(0, 0), 4.2);
    ASSERT_DOUBLE_EQ(m(0, 1), 5.);
    ASSERT_DOUBLE_EQ(m(0, 2), 23.2323);
    ASSERT_DOUBLE_EQ(m(1, 0), 0.);
    ASSERT_DOUBLE_EQ(m(1, 1), 1.55);
    ASSERT_DOUBLE_EQ(m(1, 2), 6.);
}

TEST(dense_matrix_dyn, fill) {
    matrix_dyn<double> m(2, 3);
    double arr1[2][3] {
            {4.2, 5., 23.2323},
            {0., 1.55, 6.}
    };
    for (std::size_t i = 0; i < 2; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m(i, j) = arr1[i][j];
        }
    }
    ASSERT_DOUBLE_EQ(m(0, 0), 4.2);
    ASSERT_DOUBLE_EQ(m(0, 1), 5.);
    ASSERT_DOUBLE_EQ(m(0, 2), 23.2323);
    ASSERT_DOUBLE_EQ(m(1, 0), 0.);
    ASSERT_DOUBLE_EQ(m(1, 1), 1.55);
    ASSERT_DOUBLE_EQ(m(1, 2), 6.);
}

TEST(dense_matrix, mult_div_num) {
    matrix<double, 2, 3> m{};
    double arr1[2][3] {
            {4.2, 5., 23.2323},
            {0., 1.55, 6.}
    };
    for (std::size_t i = 0; i < 2; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m(i, j) = arr1[i][j];
        }
    }

    m = m * 2.;
    ASSERT_DOUBLE_EQ(m(0, 0), 4.2*2);
    ASSERT_DOUBLE_EQ(m(0, 1), 5.*2);
    ASSERT_DOUBLE_EQ(m(0, 2), 23.2323*2);
    ASSERT_DOUBLE_EQ(m(1, 0), 0.*2);
    ASSERT_DOUBLE_EQ(m(1, 1), 1.55*2);
    ASSERT_DOUBLE_EQ(m(1, 2), (6. * 2));

    m = m / 2.;
    ASSERT_DOUBLE_EQ(m(0, 0), 4.2);
    ASSERT_DOUBLE_EQ(m(0, 1), 5.);
    ASSERT_DOUBLE_EQ(m(0, 2), 23.2323);
    ASSERT_DOUBLE_EQ(m(1, 0), 0.);
    ASSERT_DOUBLE_EQ(m(1, 1), 1.55);
    ASSERT_DOUBLE_EQ(m(1, 2), 6.);

    m = 2. * m;
    ASSERT_DOUBLE_EQ(m(0, 0), 4.2*2);
    ASSERT_DOUBLE_EQ(m(0, 1), 5.*2);
    ASSERT_DOUBLE_EQ(m(0, 2), 23.2323*2);
    ASSERT_DOUBLE_EQ(m(1, 0), 0.*2);
    ASSERT_DOUBLE_EQ(m(1, 1), 1.55*2);
    ASSERT_DOUBLE_EQ(m(1, 2), 6.*2);
}

TEST(dense_matrix_dyn, mult_div_num) {
    matrix_dyn<double> m(2, 3);
    double arr1[2][3] {
            {4.2, 5., 23.2323},
            {0., 1.55, 6.}
    };
    for (std::size_t i = 0; i < 2; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m(i, j) = arr1[i][j];
        }
    }

    m = m * 2.;
    ASSERT_DOUBLE_EQ(m(0, 0), 4.2*2);
    ASSERT_DOUBLE_EQ(m(0, 1), 5.*2);
    ASSERT_DOUBLE_EQ(m(0, 2), 23.2323*2);
    ASSERT_DOUBLE_EQ(m(1, 0), 0.*2);
    ASSERT_DOUBLE_EQ(m(1, 1), 1.55*2);
    ASSERT_DOUBLE_EQ(m(1, 2), (6. * 2));

    m = m / 2.;
    ASSERT_DOUBLE_EQ(m(0, 0), 4.2);
    ASSERT_DOUBLE_EQ(m(0, 1), 5.);
    ASSERT_DOUBLE_EQ(m(0, 2), 23.2323);
    ASSERT_DOUBLE_EQ(m(1, 0), 0.);
    ASSERT_DOUBLE_EQ(m(1, 1), 1.55);
    ASSERT_DOUBLE_EQ(m(1, 2), 6.);

    m = 2. * m;
    ASSERT_DOUBLE_EQ(m(0, 0), 4.2*2);
    ASSERT_DOUBLE_EQ(m(0, 1), 5.*2);
    ASSERT_DOUBLE_EQ(m(0, 2), 23.2323*2);
    ASSERT_DOUBLE_EQ(m(1, 0), 0.*2);
    ASSERT_DOUBLE_EQ(m(1, 1), 1.55*2);
    ASSERT_DOUBLE_EQ(m(1, 2), 6.*2);
}

TEST(dense_matrix, mult_matr) {
    matrix<double, 3, 3> m1{};
    matrix<double, 3, 3> m2{};
    double arr1[3][3] {
            {4.2, 5., 23.2323},
            {0., 1.55, 6.},
            {506., 0., 99.12}
    };
    double arr2[3][3] {
            {0., 93.01, 	12.23},
            {14., 1., 0.},
            {77., 0., 6767.}
    };
    double res[3][3] {
            {1858.8871, 395.642, 157264.3401},
            {483.7, 1.55, 40602.},
            {7632.24, 47063.06, 676933.42}
    };

    for (std::size_t i = 0; i < 3; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m1(i, j) = arr1[i][j];
        }
    }
    for (std::size_t i = 0; i < 3; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m2(i, j) = arr2[i][j];
        }
    }

    matrix<double, 3, 3> m_res{};
    m_res = m1 * m2;

    ASSERT_DOUBLE_EQ(m_res( 0, 0), res[0][0]);
    ASSERT_DOUBLE_EQ(m_res( 0, 1), res[0][1]);
    ASSERT_DOUBLE_EQ(m_res( 0, 2), res[0][2]);
    ASSERT_DOUBLE_EQ(m_res( 1, 0), res[1][0]);
    ASSERT_DOUBLE_EQ(m_res( 1, 1), res[1][1]);
    ASSERT_DOUBLE_EQ(m_res( 1, 2), res[1][2]);
    ASSERT_DOUBLE_EQ(m_res( 2, 0), res[2][0]);
    ASSERT_DOUBLE_EQ(m_res( 2, 1), res[2][1]);
    ASSERT_DOUBLE_EQ(m_res( 2, 2), res[2][2]);
}

TEST(dense_matrix_dyn, mult_matr) {
    matrix_dyn<double> m1(3, 3);
    matrix_dyn<double> m2(3, 3);
    double arr1[3][3] {
            {4.2, 5., 23.2323},
            {0., 1.55, 6.},
            {506., 0., 99.12}
    };
    double arr2[3][3] {
            {0., 93.01, 	12.23},
            {14., 1., 0.},
            {77., 0., 6767.}
    };
    double res[3][3] {
            {1858.8871, 395.642, 157264.3401},
            {483.7, 1.55, 40602.},
            {7632.24, 47063.06, 676933.42}
    };

    for (std::size_t i = 0; i < 3; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m1(i, j) = arr1[i][j];
        }
    }
    for (std::size_t i = 0; i < 3; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m2(i, j) = arr2[i][j];
        }
    }

    matrix_dyn<double> m_res = m1 * m2;

    ASSERT_DOUBLE_EQ(m_res( 0, 0), res[0][0]);
    ASSERT_DOUBLE_EQ(m_res( 0, 1), res[0][1]);
    ASSERT_DOUBLE_EQ(m_res( 0, 2), res[0][2]);
    ASSERT_DOUBLE_EQ(m_res( 1, 0), res[1][0]);
    ASSERT_DOUBLE_EQ(m_res( 1, 1), res[1][1]);
    ASSERT_DOUBLE_EQ(m_res( 1, 2), res[1][2]);
    ASSERT_DOUBLE_EQ(m_res( 2, 0), res[2][0]);
    ASSERT_DOUBLE_EQ(m_res( 2, 1), res[2][1]);
    ASSERT_DOUBLE_EQ(m_res( 2, 2), res[2][2]);
}

TEST(dense_matrix, transpose) {
    matrix<double, 2, 3> m{};
    double arr1[2][3] {
            {4.2, 5., 23.2323},
            {0., 1.55, 6.}
    };
    for (std::size_t i = 0; i < 2; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m(i, j) = arr1[i][j];
        }
    }

    matrix<double, 3, 2> m_transposed = m.transpose();
    ASSERT_DOUBLE_EQ(m_transposed(0, 0), 4.2);
    ASSERT_DOUBLE_EQ(m_transposed(0, 1), 0.);
    ASSERT_DOUBLE_EQ(m_transposed(1, 0), 5.);
    ASSERT_DOUBLE_EQ(m_transposed(1, 1), 1.55);
    ASSERT_DOUBLE_EQ(m_transposed(2, 0), 23.2323);
    ASSERT_DOUBLE_EQ(m_transposed(2, 1), 6.);
}

TEST(dense_matrix_dyn, transpose) {
    matrix_dyn<double> m(2, 3);
    double arr1[2][3] {
            {4.2, 5., 23.2323},
            {0., 1.55, 6.}
    };
    for (std::size_t i = 0; i < 2; ++i) {
        for (std::size_t j = 0; j < 3; ++j) {
            m(i, j) = arr1[i][j];
        }
    }

    matrix_dyn<double> m_transposed = m.transpose();
    ASSERT_DOUBLE_EQ(m_transposed(0, 0), 4.2);
    ASSERT_DOUBLE_EQ(m_transposed(0, 1), 0.);
    ASSERT_DOUBLE_EQ(m_transposed(1, 0), 5.);
    ASSERT_DOUBLE_EQ(m_transposed(1, 1), 1.55);
    ASSERT_DOUBLE_EQ(m_transposed(2, 0), 23.2323);
    ASSERT_DOUBLE_EQ(m_transposed(2, 1), 6.);
}

TEST(csr, fill) {
    std::set<matrix_element<double>> elements({{0, 1, 1.},
                                               {0, 2, 3.},
                                               {1, 1, 2.},
                                               {2, 0, 10.}});
    double arr1[3][3] {
            {0., 1., 3.},
            {0., 2., 0.},
            {10., 0., 0.}
    };
    csr<double> matr(3, 3, elements);
    for (auto i = 0; i < 3; ++i) {
        for (auto j = 0; j < 3; ++j) {
            EXPECT_EQ(matr(i, j), arr1[i][j]);
        }
    }
}

TEST(csr, vec_mult) {
    std::set<matrix_element<double>> elements({{0, 1, 1.},
                                               {0, 2, 3.},
                                               {1, 1, 2.},
                                               {2, 0, 10.}});
    std::vector<double> b = {1., 2., 3.};
    std::vector<double> ref = {11., 4., 10.};
    csr<double> matr(3, 3, elements);
    std::vector<double> x = matr*b;
    for (auto i = 0; i < 3; ++i) {
        EXPECT_EQ(x[i], ref[i]);
    }
}

TEST(csr, num_mult) {
    std::set<matrix_element<double>> elements({{0, 1, 1.},
                                               {0, 2, 3.},
                                               {1, 1, 2.},
                                               {2, 0, 10.}});
    std::vector<double> nums = {1., 2., 3., 4., 5., 105., 0.1235};
    double arr1[3][3] {
            {0., 1., 3.},
            {0., 2., 0.},
            {10., 0., 0.}
    };
    csr<double> matrix(3, 3, elements);
    for (auto num : nums) {
        csr<double> test = matrix*num;
        for (auto i = 0; i < 3; ++i) {
            for (auto j = 0; j < 3; ++j) {
                ASSERT_DOUBLE_EQ(test(i, j), (arr1[i][j]*num));
            }
        }
    }
}

TEST(vec_operations, minus) {
    std::vector<double> first = {1., 2., 3., 4., 5., 6., 7., 8., 9., 10.};
    std::vector<double> second = {2., 3., 4., 5., 6., 7., 8., 9., 10., 11.};
    auto res = second - first;
    for (std::size_t i = 0; i < 10; ++i) {
        ASSERT_DOUBLE_EQ(res[i], second[i] - first[i]);
    }
}

TEST(vec_operations, plus) {
    std::vector<double> first = {1., 2., 3., 4., 5., 6., 7., 8., 9., 10.};
    std::vector<double> second = {2., 3., 4., 5., 6., 7., 8., 9., 10., 11.};
    auto res = second + first;
    for (std::size_t i = 0; i < 10; ++i) {
        ASSERT_DOUBLE_EQ(res[i], second[i] + first[i]);
    }
}

TEST(vec_operations, mult_vec) {
    std::vector<double> first = {1., 2., 3., 4., 5., 6., 7., 8., 9., 10.};
    std::vector<double> second = {2., 3., 4., 5., 6., 7., 8., 9., 10., 11.};
    double res = second * first;
    double tmp = 0;
    for (auto i = 0; i < first.size(); ++i) {
        tmp += second[i] * first[i];
    }
    ASSERT_DOUBLE_EQ(tmp, res);
}

TEST(vec_operations, mult_num) {
    std::vector<double> first = {1., 2., 3., 4., 5., 6., 7., 8., 9., 10.};
    std::vector<double> res = 2.1234 * first;
    for (auto i = 0; i < first.size(); ++i) {
        ASSERT_DOUBLE_EQ(res[i], first[i]*2.1234);
    }
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
