#pragma once

#include "types/csr_matrix.h"
#include "types/vec_operations.h"
#include <cstdlib>

namespace SLAE_mini{ 

template<typename T>
std::vector<T> SOR(const csr<T> &A, const std::vector<T> &b,
                                   const std::vector<T> &x0, T w, T r);

template<typename T>
std::vector<T> SOR(const csr<T> &A, const std::vector<T> &b,
                                   const std::vector<T> &x0, T w, T r) {
    std::size_t N = x0.size();
    std::vector<T> x = x0;
    T sum;
    const std::vector<T> &vals = A.getVals();
    const std::vector<std::size_t> &rows = A.getRows();
    const std::vector<std::size_t> &cols = A.getCols();

    while(norm(A * x - b) > r ) {
        for (std::size_t k = 0; k < N; ++k) {
            sum = 0.;
            T Ajj;
            for (std::size_t j = rows[k]; j < rows[k+1]; ++j) {
                if (cols[j] != k) sum += vals[j] * x[cols[j]];
                else Ajj = vals[j];
            }
            x[k] = (1.-w)*x[k] +  w / Ajj * (b[k] - sum );
        }
    }
    return x;
}

} // namespace SLAE_mini
