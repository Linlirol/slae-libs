#pragma once

#include "types/dense_matrix.h"
#include <cstdlib>
#include "types/csr_matrix.h"
#include <vector>

namespace SLAE_mini {

template<typename T>
constexpr std::vector<T> CG(const csr<T> &A, const std::vector<T> &b,
                            const std::vector<T> &x0, T r);

template<typename T>
constexpr std::vector<T> CG(const csr<T> &A, const std::vector<T> &b,
                             const std::vector<T> &x0, T r) {
    std::vector<T> x = x0;
    std::vector<T> residual = A*x - b;
    std::vector<T> d = residual;
    std::size_t N = b.size();
    T r2 = dot(residual, residual);

    while (norm(residual) > r) {
        for (std::size_t i = 0; i < N; ++i) {
            std::vector<T> Ad = A * d;
            T alpha = dot(residual, residual) / SLAE_mini::dot(d, Ad);
            x = x - alpha * d;
            residual = residual - alpha * Ad;
            if (norm(residual) < r) break;
            else {
                T tmp = dot(residual, residual);
                d = residual + (tmp / r2) * d;
                r2 = tmp;
            }
        }
    }
    return x;
}

} // namespace SLAE_mini
