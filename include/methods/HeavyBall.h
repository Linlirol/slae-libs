#pragma once

#include "types/dense_matrix.h"
#include <cstdlib>
#include "types/csr_matrix.h"
#include <vector>

namespace SLAE_mini {

template<typename T>
constexpr std::vector<T> HeavyBall(const csr<T> &A, const std::vector<T> &b,
                            const std::vector<T> &x0, T r);

template<typename T>
constexpr std::vector<T> HeavyBall(const csr<T> &A, const std::vector<T> &b,
                            const std::vector<T> &x0, T r) {
    std::vector<T> x = x0;
    std::vector<T> x_prev = x0;
    std::vector<T> y = x0;
    std::vector<T> residual = A*x - b;

    T alpha = dot(residual, residual) / dot(residual, A*residual);
    T beta; std::vector<T> delta, Ares;

    while(norm(residual) > r) {
        x = y - alpha * residual;
        delta = x - x_prev;
        residual = A*x - b;
        Ares = A*residual;
        beta = dot(residual, alpha * Ares - residual) / dot(delta, Ares);
        y = x + beta*delta;
        alpha = ( beta * dot(delta, Ares) + dot(residual, residual) ) /
                dot(residual, Ares);
        x_prev = x;
    }
    return x;
}
} // namespace SLAE_mini
