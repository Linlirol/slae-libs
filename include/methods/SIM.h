#pragma once

#include "types/dense_matrix.h"
#include <cstdlib>
#include "types/csr_matrix.h"
#include <vector>

namespace SLAE_mini {

template<typename T>
constexpr std::vector<T> SIM(const csr<T> &A, const std::vector<T> &b,
                             const std::vector<T> &x0, T r, T tau);

template<typename T>
constexpr std::vector<T> SIM(const csr<T> &A, const std::vector<T> &b,
                             const std::vector<T> &x0, T r, T tau) {
    std::vector<T> x = x0;
    std::vector<T> Axb = A*x - b;
    while( norm(Axb ) > r ) {
        x = x - tau*(Axb);
        Axb = A*x - b;
    }
    return x;
}


template<typename T, std::size_t N>
constexpr matrix<T, N, 1> SIM(const matrix<T, N, N> &A, const matrix<T, N, 1> &b,
                              const matrix<T, N, 1> &x0, T r, T tau);

template<typename T, std::size_t N>
constexpr matrix<T, N, 1> SIM(const matrix<T, N, N> &A, const matrix<T, N, 1> &b,
                              const matrix<T, N, 1> &x0, T r, T tau) {
    matrix<T, N, 1> x = x0;
    // P and C matrix specification
    matrix<T, N, N> P = -tau * A;
    matrix<T, N, 1> c;
    for (std::size_t i = 0; i < N; ++i) {
        P(i, i) += 1.;
        c(i, 0) = b(i, 0) * tau;
    }

    // Iteration method implementation
    while( (A*x - b).first_norm() > r ) {
        x = P * x + c;
    }
    return x;
}

} // namespace SLAE_mini
