#pragma once

#include "types/csr_matrix.h"
#include <vector>
#include <cstdlib>
#include <cmath>
#include <array>

namespace SLAE_mini {

// Массив индексов tau
template <std::size_t i>
constexpr std::array<std::size_t, i> ChebyshevShake() {
    static_assert(i % 2 == 0);
    std::array<std::size_t, i / 2> prev_row = ChebyshevShake<i / 2>();
    std::array<std::size_t, i> row{};
    for (std::size_t j = 0; j < prev_row.size(); ++j) {
        row[j * 2] = prev_row[j];
        row[j * 2 + 1] = i - prev_row[j] - 1;
    }
    return row;
}

template <>
constexpr std::array<std::size_t, 2> ChebyshevShake<2>() {
    return {0, 1};
}

// Корни полинома Чебышева
template<typename T>
std::vector<T> ChebyshevCosines(std::size_t i) {
    std::vector<T> cosines(i);
    cosines[0] = std::cos(M_PI / (2.*i)); cosines[i-1] = - cosines[0];
    // x_{k+1} = x*cos(pi/i) - sqrt{1-x^2}*sin(pi/i)
    // x - k-й корень полинома
    T c1 = std::cos(M_PI/static_cast<T>(i));
    T c2 = std::sin(M_PI/static_cast<T>(i));
    for (std::size_t j = 1; j < i/2; ++j) {
        cosines[j] = cosines[j-1] * c1 - std::sqrt(1 - cosines[j-1]*cosines[j-1]) * c2;
        cosines[i - j - 1] = - cosines[j];
    }
    return cosines;
}

template<typename T>
constexpr std::vector<T> ChebyshevSIM(const csr<T> &A, const std::vector<T> &b,
                                    const std::vector<T> &x0, T r, T lambda_max, T lambda_min);

template<typename T>
constexpr std::vector<T> ChebyshevSIM(const csr<T> &A, const std::vector<T> &b,
                                      const std::vector<T> &x0, T r, T lambda_max, T lambda_min) {
    constexpr std::size_t i = 64;
    std::array<std::size_t, i> t_order = ChebyshevShake<i>();
    std::vector<T> cosines = ChebyshevCosines<T>(i);

    T c1 = (lambda_max + lambda_min) / 2.;
    T c2 = (lambda_max - lambda_min) / 2.;

    std::vector<T> x = x0;
    std::vector<T> Axb = A*x - b;
    while( norm(Axb) > r ) {
        for (std::size_t k = 0; k < i; ++k) {
            x = x - Axb/(c1 + c2 * cosines[t_order[k]]);
            Axb = A*x - b;
        }
    }
    return x;
}

} // namespace SLAE_mini
