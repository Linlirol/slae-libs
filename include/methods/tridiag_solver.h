#pragma once

#include <array>
#include "types/tridiag_matrix.h"

namespace SLAE_mini {

/** Thomas algorithm for SLAE with the tridiagonal matrix */

template<typename T, std::size_t N>
constexpr std::array<T, N> tridiag_solver(const tridiag_matrix<T, N> &matrA, const std::array<T, N> &f);

template<typename T>
constexpr std::vector<T> tridiag_solver_dyn(const tridiag_matrix_dyn<T> &matrA, const std::vector<T> &f);

/**  Implementation of solver function  **/

template<typename T, std::size_t N>
constexpr std::array<T, N> tridiag_solver(const tridiag_matrix<T, N> &matrA, const std::array<T, N> &f) {
    std::array<T, N> x;
    std::array<T, N-1> p, q;
    p[0] = - matrA(2*N-1) / matrA(N-1);
    q[0] = f[0]/matrA(N-1);
    T comm;
    for (std::size_t i = 1; i < N-1; ++i) {
        comm = (matrA(i-1)*p[i-1] + matrA(N-1+i));
        p[i] = - matrA(2*N-1 + i)/comm;
        q[i] = (f[i] - matrA(i-1)*q[i-1])/comm;
    }

    x[N-1] = (f[N-1] - matrA(N-2)*q[N-2])/(matrA(N-2)*p[N-2] + matrA(2*N-2));

    std::size_t bias = N-2;
    for (std::size_t i = 0; i <= bias; ++i) {
        x[bias - i] = p[bias - i]*x[bias - i+1] + q[bias - i];
    }

    return x;
}

template<typename T>
constexpr std::vector<T> tridiag_solver(const tridiag_matrix_dyn<T> &matrA, const std::vector<T> &f) {
    std::size_t N = f.size();
    std::vector<T> x(N);
    std::vector<T> p(N), q(N);
    p[0] = - matrA(2*N-1) / matrA(N-1);
    q[0] = f[0]/matrA(N-1);
    T comm;
    for (std::size_t i = 1; i < N-1; ++i) {
        comm = (matrA(i-1)*p[i-1] + matrA(N-1+i));
        p[i] = - matrA(2*N-1 + i)/comm;
        q[i] = (f[i] - matrA(i-1)*q[i-1])/comm;
    }

    x[N-1] = (f[N-1] - matrA(N-2)*q[N-2])/(matrA(N-2)*p[N-2] + matrA(2*N-2));

    std::size_t bias = N-2;
    for (std::size_t i = 0; i <= bias; ++i) {
        x[bias - i] = p[bias - i]*x[bias - i+1] + q[bias - i];
    }

    return x;
}

} // namespace SLAE_mini
