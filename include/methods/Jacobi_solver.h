#pragma once

#include"types/dense_matrix.h"
#include<cstdlib>
#include<iostream>

namespace SLAE_mini {

template<typename T>
std::vector<T> jacobi_solver(const csr<T> &A, const std::vector<T> &b,
                                   const std::vector<T> &x0, T r);

template<typename T>
std::vector<T> jacobi_solver(const csr<T> &A, const std::vector<T> &b,
                                   const std::vector<T> &x0, T r) {
    std::size_t N = x0.size();
    std::vector<T> x1 = x0;
    std::vector<T> x2(N);
    T sum;
    const std::vector<T> &vals = A.getVals();
    const std::vector<std::size_t> &rows = A.getRows();
    const std::vector<std::size_t> &cols = A.getCols();

    while(norm(A * x1 - b) > r ) {
        for (std::size_t k = 0; k < N; ++k) {
            sum = 0;
            T Ajj;
            for (std::size_t j = rows[k]; j < rows[k+1]; ++j) {
                if (cols[j] != k) sum += vals[j] * x1[cols[j]];
                else Ajj = vals[j];
            }
            x2[k] = 1 / Ajj * (b[k] - sum );
        }
        x1 = x2;
    }
    return x1;
}


template<typename T, std::size_t N>
constexpr matrix<T, N, 1> jacobi_solver(const matrix<T, N, N> &A, const matrix<T, N, 1> &b,
                                        const matrix<T, N, 1> &x0, T r);

template<typename T, std::size_t N>
constexpr matrix<T, N, 1> jacobi_solver(const matrix<T, N, N> &A, const matrix<T, N, 1> &b,
                                        const matrix<T, N, 1> &x0, T r) {
    matrix<T, N, 1> x1;
    matrix<T, N, 1> x2 = x0;
    T sum;
    while( (A*x2 - b).first_norm() > r ) {
        x1 = x2;
        for (std::size_t k = 0; k < N; ++k) {
            sum = 0;
            for (std::size_t j = 0; j < k; ++j) {
                sum += A(k, j) * x1(j, 0);
            }
            for (std::size_t j = k+1; j < N; ++j) {
                sum += A(k, j) * x1(j, 0);
            }
            x2(k, 0) = ( b(k, 0) - sum )/A(k, k);
        }
    }
    return x2;
}

} // namespace SLAE_mini
