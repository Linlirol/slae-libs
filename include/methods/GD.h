#pragma once

#include <cstdlib>
#include "types/csr_matrix.h"
#include <vector>
#include "types/vec_operations.h"

namespace SLAE_mini {

template<typename T>
constexpr std::vector<T> GD(const csr<T> &A, const std::vector<T> &b,
                                    const std::vector<T> &x0, T r, T tau);

template<typename T>
constexpr std::vector<T> GD(const csr<T> &A, const std::vector<T> &b,
                                    const std::vector<T> &x0, T r) {
    std::vector<T> x = x0;
    std::vector<T> residual = A*x - b;
    T alpha;
    while( norm(residual ) > r ) {
        alpha = dot(residual, residual) / dot(residual, A * residual);
        x = x - alpha*(residual);
        residual = A*x - b;
    }
    return x;
}

} // namespace SLAE_mini
