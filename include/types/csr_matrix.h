#pragma once

#include<vector>
#include<set>
#include "types/vec_operations.h"

namespace SLAE_mini {

template <typename T>
struct matrix_element {
    std::size_t i; // Индекс строки [0, M]
    std::size_t j; // Индекс столбца [0, N]
    T value;
    bool operator<(const matrix_element &r) const {
        return i < r.i || (i == r.i && j < r.j);
    }

};

template<typename T>
class csr {
private:
    std::size_t row_num;
    std::size_t col_num;
    std::vector<T> values; // Ненулевые значения
    std::vector<std::size_t> cols; // Массив индексов столбцов
    std::vector<std::size_t> rows; // Массив индексации строк
public:
    csr(std::size_t row_, std::size_t col_, const std::set<matrix_element<T>> &elements);

    T operator()(std::size_t i, std::size_t j) const;
    std::vector<T> operator*(std::vector<T> vec) const;
    csr<T> operator*(T num) const;
    T norm() const;

    [[nodiscard]] const std::vector<T> &getVals() const { return  values; }
    [[nodiscard]] const std::vector<std::size_t> &getCols() const { return  cols; }
    [[nodiscard]] const std::vector<std::size_t> &getRows() const { return  rows; }

    [[nodiscard]] std::size_t getRowNum() const { return row_num; }
    [[nodiscard]] std::size_t getColNum() const {return col_num; }

};

template<typename T>
T csr<T>::norm() const { // max{|Aij|}
    T result = 0;
    T tmp;
    for (auto &val : values) {
        tmp = std::abs(val);
        if (result < tmp) result = tmp;
    }
    return result;
}

template <typename T>
csr<T> csr<T>::operator*(T num) const {
    csr<T> res = *this;
    for (auto &i : res.values) {
        i *= num;
    }
    return res;
}

template <typename T>
std::vector<T> csr<T>::operator*(std::vector<T> vec) const {
    std::vector<T> result(row_num, 0);
    for (std::size_t i = 0; i < row_num; ++i) {
        for (std::size_t k = rows[i]; k < rows[i+1]; ++k) {
            result[i] += values[k] * vec[cols[k]];
        }
    }
    return result;
}

template <typename T>
csr<T>::csr(std::size_t row_, std::size_t col_, const std::set<matrix_element<T>> &elements) :
                                                            row_num(row_),
                                                            col_num(col_),
                                                            rows(std::vector<std::size_t>(row_+1)),
                                                            cols(std::vector<std::size_t>(elements.size())),
                                                            values(std::vector<T>(elements.size())){
    std::size_t k = 0;
    for (auto &el : elements) {
        values[k] = el.value;
        cols[k] = el.j;
        rows[el.i+1]++;
        ++k;
    }
    for (std::size_t m = 1; m < rows.size(); ++m) rows[m] += rows[m-1];
}

template <typename T>
T csr<T>::operator()(std::size_t i, std::size_t j) const {
    for (std::size_t k = rows[i]; k < rows[i+1]; ++k) {
        if (cols[k] == j) {
            return values[k];
        }
    }
    return 0;
}

} // namespace SLAE_mini
