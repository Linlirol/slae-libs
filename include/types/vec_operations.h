#pragma once

#include <cstdlib>
#include <vector>

namespace SLAE_mini {

template <typename T>
std::vector<T> operator+(const std::vector<T> &first, const std::vector<T> &second) {
    std::vector<T> res(first.size());
    for (std::size_t i = 0; i < first.size(); ++i) {
        res[i] = first[i] + second[i];
    }
    return res;
}

template <typename T>
std::vector<T> operator-(const std::vector<T> &first, const std::vector<T> &second) {
    std::vector<T> res(first.size());
    for (std::size_t i = 0; i < first.size(); ++i) {
        res[i] = first[i] - second[i];
    }
    return res;
}

template <typename T>
T operator*(const std::vector<T> &first, const std::vector<T> &second) {
    T res = 0.;
    for (std::size_t i = 0; i < first.size(); ++i) res += first[i]*second[i];
    return res;
}

template <typename T>
std::vector<T> operator*(const std::vector<T> &first, T num) {
    std::vector<T> res(first.size());
    for (std::size_t i = 0; i < first.size(); ++i) res[i] = first[i] * num;
    return res;
}

template <typename T>
std::vector<T> operator*(T num, const std::vector<T> &first) { return first * num; }


template <typename T>
std::vector<T> operator/(const std::vector<T> &first, T num) {
    std::vector<T> res(first.size());
    for (std::size_t i = 0; i < first.size(); ++i) res[i] = first[i] / num;
    return res;
}

template <typename T>
T norm(const std::vector<T> &vec)  {
    T res = 0.;
    T tmp;
    for (auto el : vec) {
        tmp = std::abs(el);
        if(res < tmp) res = tmp;
    }
    return res;
}

template <typename T>
T dot(const std::vector<T> &first, const std::vector<T> &second)  {
    T res = 0.;
    for (std::size_t i = 0; i < first.size(); ++i) {
        res += first[i] * second[i];
    }
    return res;
}

} // namespace SLAE_mini
