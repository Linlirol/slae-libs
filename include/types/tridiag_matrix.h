#pragma once

#include<array>

namespace SLAE_mini {

/** Tridiagonal matrix representation as 1d array \ vector */

template<typename T>
class tridiag_matrix_dyn {
protected:
    const std::size_t size_;
    std::vector<T> matrA;
public:
    tridiag_matrix_dyn(std::size_t size_,const std::vector<T> &a, const std::vector<T> &b,
                       const std::vector<T> &c);
    T operator()(std::size_t i) const;
};

template <typename T>
tridiag_matrix_dyn<T>::tridiag_matrix_dyn(std::size_t size_, const std::vector<T> &a, const std::vector<T> &b,
                                          const std::vector<T> &c) : size_(size_), matrA(std::vector<T>(3*size_-2)) {
    for (std::size_t i = 0; i < size_-1; ++i) {
        matrA[i] = a[i];
        matrA[size_-1+i] = b[i];
        matrA[2*size_-1 + i] = c[i];
    }
    matrA[2*size_-2] = b[size_-1];
}

template <typename T>
T tridiag_matrix_dyn<T>::operator()(std::size_t i) const {
    return matrA[i];
}

template<typename T, std::size_t N>
class tridiag_matrix {
protected:
    std::array<T, 3*N-2> matrA; // [a, b, c]
public:
    constexpr tridiag_matrix(const std::array<T, N-1> &a, const std::array<T, N> &b,
                             const std::array<T, N-1> &c);
    T operator()(std::size_t i) const;
};

template<typename T, std::size_t N>
constexpr tridiag_matrix<T, N>::tridiag_matrix(const std::array<T, N-1> &a, const std::array<T, N> &b,
                                               const std::array<T, N-1> &c) {
    for (std::size_t i = 0; i < N-1; ++i) {
        matrA[i] = a[i];
        matrA[N-1+i] = b[i];
        matrA[2*N-1 + i] = c[i];
    }
    matrA[2*N-2] = b[N-1];
}

template<typename T, std::size_t N>
T tridiag_matrix<T, N>::operator()(std::size_t i) const {
    return matrA[i];
}

} // namespace SLAE_mini
