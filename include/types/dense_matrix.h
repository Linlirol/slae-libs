#pragma once

#include<array>
#include<vector>
#include<cmath>

namespace SLAE_mini {

/** Matrix representation as 1d array \ vector */
template<typename T>
class matrix_dyn {
protected:
    std::size_t row_num;
    std::size_t column_num;
    std::vector<T> elements;
public:
    matrix_dyn(std::size_t row_, std::size_t column_) : row_num(row_), column_num(column_),
                                                        elements(std::vector<T>(row_*column_)) {}

    const T &operator()(std::size_t i, std::size_t j) const { return elements[i*column_num + j]; };
    T &operator()(std::size_t i, std::size_t j) { return elements[i*column_num + j]; };

    [[nodiscard]] constexpr std::size_t rowNum() const { return row_num; } // M
    [[nodiscard]] constexpr std::size_t columnNum() const { return column_num; } // N

    matrix_dyn<T> operator+(const matrix_dyn<T> &sec) const;
    matrix_dyn<T> operator-(const matrix_dyn<T> &sec) const;

    matrix_dyn<T> operator*(const T &num) const;

    matrix_dyn<T> operator/(const T &num) const;

    matrix_dyn<T> operator*(const matrix_dyn<T> &sec) const;

    matrix_dyn<T> transpose() const;

    T first_norm() const;
};

template <typename T>
matrix_dyn<T> matrix_dyn<T>::operator+(const matrix_dyn<T> &sec) const {
    matrix_dyn<T> res(row_num, column_num);
    for (std::size_t i = 0; i < column_num*row_num; ++i) {
        res.elements[i] = elements[i] + sec.elements[i];
    }
    return res;
}

template <typename T>
matrix_dyn<T> matrix_dyn<T>::operator-(const matrix_dyn<T> &sec) const {
    matrix_dyn<T> res(row_num, column_num);
    for (std::size_t i = 0; i < column_num*row_num; ++i) {
        res.elements[i] = elements[i] - sec.elements[i];
    }
    return res;
}

template <typename T>
matrix_dyn<T> matrix_dyn<T>::operator*(const T &num) const {
    matrix_dyn<T> res(row_num, column_num);
    for (std::size_t i = 0; i < column_num*row_num; ++i) {
        res.elements[i] = elements[i] * num;
    }
    return res;
}

template <typename T>
matrix_dyn<T> operator*(const T &num, const matrix_dyn<T> &sec) {
    return sec * num;
}

template <typename T>
matrix_dyn<T> matrix_dyn<T>::operator/(const T &num) const {
    matrix_dyn<T> res(row_num, column_num);
    for (std::size_t i = 0; i < column_num*row_num; ++i) {
        res.elements[i] = elements[i] / num;
    }
    return res;
}

template <typename T>
matrix_dyn<T> matrix_dyn<T>::operator*(const matrix_dyn<T> &sec) const {
    matrix_dyn<T> res(row_num, sec.column_num);
    T s;
    for (std::size_t i = 0; i < row_num; ++i) {
        for (std::size_t j = 0; j < sec.column_num; ++j) {
            s = 0.;
            for (std::size_t k = 0; k < column_num; ++k) {
                s += (*this)(i, k) * sec(k, j);
            }
            res(i, j) = s;
        }
    }
    return res;
}

template <typename T>
matrix_dyn<T> matrix_dyn<T>::transpose() const {
    matrix_dyn<T> res(column_num, row_num);
    for (std::size_t i = 0; i < row_num; ++i) {
        for (std::size_t j = 0; j < column_num; ++j) {
            res(j, i) = elements[i*column_num + j];
        }
    }
    return res;
}

template <typename T>
T matrix_dyn<T>::first_norm() const {
    T max_sum = 0.;
    T sum = 0.;
    for (std::size_t j = 0; j < column_num; ++j) {
        for (std::size_t i = 0; i < row_num; ++i) {
            sum += std::abs((*this)(i, j));
        }
        if (sum > max_sum) max_sum = sum;
        sum = 0.;
    }
    return max_sum;
}

template<typename T, std::size_t M, std::size_t N>
class matrix {
protected:
    std::array<T, N*M> elements;
public:
    constexpr const T &operator()(std::size_t i, std::size_t j) const;
    constexpr T &operator()(std::size_t i, std::size_t j);

    [[nodiscard]] constexpr std::size_t rowNum() const { return M; }
    [[nodiscard]] constexpr std::size_t columnNum() const { return N; }

    constexpr matrix<T, M, N> operator+(const matrix<T, M, N> &sec) const;
    constexpr matrix<T, M, N> operator-(const matrix<T, M, N> &sec) const;

    constexpr matrix<T, M, N> operator*(const T &num) const;

    constexpr matrix<T, M, N> operator/(const T &num) const;

    template<std::size_t P>
    constexpr matrix<T, M, P> operator*(const matrix<T, N, P> &sec) const;

    constexpr matrix<T, N, M> transpose() const;

    constexpr T first_norm() const;
};

template <typename T, std::size_t M, std::size_t N>
constexpr matrix<T, M, N> operator*(const T &num, const matrix<T, M, N> &sec) {
    return sec * num;
}

template <typename T, std::size_t M, std::size_t N>
constexpr const T &matrix<T, M, N>::operator()(std::size_t i, std::size_t j) const {
    return elements[i*N + j];
}

template <typename T, std::size_t M, std::size_t N>
constexpr T &matrix<T, M, N>::operator()(std::size_t i, std::size_t j) {
    return elements[i*N + j];
}

template <typename T, std::size_t M, std::size_t N>
constexpr matrix<T, M, N> matrix<T, M, N>::operator+(const matrix<T, M, N> &sec) const {
    matrix<T, M, N> res;
    for (std::size_t i = 0; i < M*N; ++i) {
        res.elements[i] = elements[i] + sec.elements[i];
    }
    return res;
}

template <typename T, std::size_t M, std::size_t N>
constexpr matrix<T, M, N> matrix<T, M, N>::operator-(const matrix<T, M, N> &sec) const {
    matrix<T, M, N> res;
    for (std::size_t i = 0; i < M*N; ++i) {
        res.elements[i] = elements[i] - sec.elements[i];
    }
    return res;
}

template <typename T, std::size_t M, std::size_t N>
constexpr matrix<T, M, N> matrix<T, M, N>::operator*(const T &num) const {
    matrix<T, M, N> res;
    for (std::size_t i = 0; i < M*N; ++i) {
        res.elements[i] = elements[i] * num;
    }
    return res;
}

template <typename T, std::size_t M, std::size_t N>
constexpr matrix<T, M, N> matrix<T, M, N>::operator/(const T &num) const {
    matrix<T, M, N> res;
    for (std::size_t i = 0; i < M*N; ++i) {
        res.elements[i] = elements[i] / num;
    }
    return res;
}

template <typename T, std::size_t M, std::size_t N>
template<std::size_t P>
constexpr matrix<T, M, P> matrix<T, M, N>::operator*(const matrix<T, N, P> &sec) const {
    matrix<T, M, P> res;
    T s;
    for (std::size_t i = 0; i < M; ++i) {
        for (std::size_t j = 0; j < P; ++j) {
            s = 0.;
            for (std::size_t k = 0; k < N; ++k) {
                s += (*this)(i, k) * sec(k, j);
            }
            res(i, j) = s;
        }
    }
    return res;
}

template <typename T, std::size_t M, std::size_t N>
constexpr matrix<T, N, M> matrix<T, M, N>::transpose() const {
    matrix<T, N, M> res;
    for (std::size_t i = 0; i < M; ++i) {
        for (std::size_t j = 0; j < N; ++j) {
            res(j, i) = elements[i*N + j];
        }
    }
    return res;
}

template <typename T, std::size_t M, std::size_t N>
constexpr T matrix<T, M, N>::first_norm() const {
    T max_sum = 0.;
    T sum = 0.;
    for (std::size_t j = 0; j < N; ++j) {
        for (std::size_t i = 0; i < M; ++i) {
            sum += std::abs((*this)(i, j));
        }
        if (sum > max_sum) max_sum = sum;
        sum = 0.;
    }
    return max_sum;
}

} // namespace SLAE_mini
