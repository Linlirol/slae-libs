#pragma once

#include <types/csr_matrix.h>
#include <types/dense_matrix.h>
#include <types/tridiag_matrix.h>
#include <types/vec_operations.h>

#include <methods/CG.h>
#include <methods/GD.h>
#include <methods/Jacobi_solver.h>
#include <methods/SOR.h>
#include <methods/gauss_seidel_solver.h>
#include <methods/Chebyshev_SIM.h>
#include <methods/HeavyBall.h>
#include <methods/SIM.h>
#include <methods/SSOR.h>
#include <methods/tridiag_solver.h>
