# SLAE Methods and Classes

## CSR Matrix
This is a library for working with Compressed Sparse Row (CSR) matrices.

Features:
* Efficient storage of sparse matrices
* Fast matrix-vector multiplication
* Support for solving linear systems of equations using iterative methods

Usage example:
```
std::set<matrix_element<double>> elements({{0, 1, 1.},
                                           {0, 2, 3.},
                                           {1, 1, 2.},
                                           {2, 0, 10.}});
csr<double> matr(3, 3, elements);
// 0  1 3
// 0  2 0
// 10 0 0
```

## Dense Matrix
The dense_matrix library provides a class for representing matrix as a 1D array, and implements math operations with matrix.

## Tridiagonal Matrix 
The tridiag_matrix library provides a class for representing tridiagonal matrices, and implements some operations.

## Tridiagonal Solver 
The tridiag_solver library provides a class for solving linear systems of equations represented by tridiagonal matrices. It uses the Thomas algorithm, which is an efficient method for solving tridiagonal systems.

## Simple Iteration Method Solver
Simple Iteration Method Solver implements the formula:
```math
\mathbf{x}^{i+1} = \mathbf{x}^{i} - \tau \cdot (\mathbf{Ax}^{i} - \mathbf{b})
```

The ChebyshevSIM function implements the Chebyshev acceleration of the simple iteration method for positively definite symmetric matrices. The k-th value of the coefficient $\tau$ is calculated using the following formula:
```math
\frac{1}{\tau_{k}} = \frac{\lambda_{max} + \lambda_{min}}{2} + \frac{\lambda_{max} - \lambda_{min}}{2}\cdot \cos(\frac{\pi (2k-1)}{2i})
```
The values of $\lambda_{max}$ and $\lambda_{min}$ are the maximum and minimum eigenvalues of matrix A.
To reduce the calculation error, these values are shuffled using the ChebyshevShake function.


And stopped when the residual norm $||\mathbf{Ax} - \mathbf{b}||$ becomes lower than $r$ parameter.

## Jacobi Solver
Jacobi solver implements the formula:
```math
 \mathbf{x}^{i+1}_{k} = \frac{1}{\mathbf{A}_{kk}} (\mathbf{b}_{k} - \sum_{j\neq k}\mathbf{A}_{kj}\mathbf{x}^{i}_{j}) 
```
And stopped when the residual norm $||\mathbf{Ax} - \mathbf{b}||$ becomes lower than $r$ parameter.

## Gauss-Seidel Solver
Gauss-Seidel Solver implements the formula:
```math
\mathbf{x}^{i+1}_{k} = \frac{1}{\mathbf{A}_{kk}} (\mathbf{b}_{k} - \sum_{j = 1}^{k-1}\mathbf{A}_{kj}\mathbf{x}^{i+1}_{j} - \sum_{j = k+1}^{n}\mathbf{A}_{kj}\mathbf{x}^{i}_{j}) 
```
And stopped when the residual norm $||\mathbf{Ax} - \mathbf{b}||$ becomes lower than $r$ parameter.

## (Symmetric) Successive Over-Relaxation Method
SOR solver implements the formula:
```math
\mathbf{x}^{i+1}_{k} = (1-\omega)\cdot \mathbf{x_{k}^+{i}} + \frac{\omega}{\mathbf{A}_{kk}} (\mathbf{b}_{k} - \sum_{j = 1}^{k-1}\mathbf{A}_{kj}\mathbf{x}^{i+1}_{j} - \sum_{j = k+1}^{n}\mathbf{A}_{kj}\mathbf{x}^{i}_{j}) 
```


----
## Requirements
* A C++17 compiler
* cmake 3.20+



## Building and Testing

To build the project from source:

``` 
git clone https://gitlab.com/Linlirol/slae-libs.git
cd slae-libs
mkdir build
cd build
cmake -DTESTS=ON ../
make
  ```
To disable testing:
```
cmake -DTESTS=OFF ../
```

The project includes tests for all libraries, written using the Google Test framework. Tests are divided into groups. To run the tests, use the following command in the build directory:
```
cd tests
./run_tests_solvers
./run_tests_types
```
---
[Google Test](https://github.com/google/googletest)