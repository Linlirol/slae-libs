cmake_minimum_required(VERSION 3.20)
set(CMAKE_CXX_STANDARD 17)
project(SLAE)

add_subdirectory(include)

OPTION(TESTS "Enable testing of the project" OFF)
if (TESTS)
    enable_testing()
    add_subdirectory(tests)
    message("TESTING ENABLED")
else()
    MESSAGE("TESTING DISABLED")
endif()
